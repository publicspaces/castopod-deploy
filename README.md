# Castopod

Castopod is an online podcast service. The service is split into 3 containers, a MySQL database service, a container that handles PHP processing (`back`) and a container that serves static content (`front`).

This repo is a modified and translated clone of: https://gitlab.utc.fr/picasoft/projets/services/castopod

## Update

Just update the version number in the `docker-compose.yml` file and in the `Dockerfile` of the `front` **and** of the `back`. Then run a `docker-compose build`.

**Warning**: The software is still in *beta* the database migrations are not yet done automatically, check the release notes and do them if necessary.

## Installation

Fill in the files in `secrets` and run it. Go to `/cp-auth` to finish the installation and create the administrator account. Then to log in go to `/cp-auth/login`, the admin interface is available at `/cp-admin`.

You should also remember to set the `CP_BASEURL` variable on the backend indicating the base URL of the instance, in this case `https://podcast.publicspaces.net`.
